def solution_1(file_name):
    try:
        with open(file_name, "r") as file:
            file_content = file.read()

        string_list = file_content.splitlines()

        first = ""
        last = ""
        numbers = []

        for string in string_list:
            for char in string:
                if char.isdigit():
                    first = char
                    break

            for char in reversed(string):
                if char.isdigit():
                    last = char
                    break

            numbers.append(int(first + last))
        return sum(numbers)

    except FileNotFoundError:
        return "The file doesn't exist."

result = solution_1("input.txt")
print(result)
